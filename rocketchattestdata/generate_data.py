#!/usr/bin/env python
"""Generate test data for RocketChat.

Attributes:
    DATE_2022 (str): Description
    DATE_2023 (str): Description
    DATE_2024 (str): Description
    ROLES (list): Description
"""


import datetime
import json
import pathlib
import random
import string
from dataclasses import dataclass
from pathlib import Path
from typing import List, Union

import click
from names_generator import generate_name


# have some random date here for these and then make sure every time
# that the random date makes sense before creating mock data
DATE_2022 = "1648294815"
DATE_2023 = "1679830815"
DATE_2024 = "1712221754"

ROLES = ["user", "admin"]


GLOB_COUNT = 0


def random_string(length: int) -> str:
    """Generates a random string of specified length.

    Args:
        length (int): The length of the random string to generate.

    Returns:
        str: A randomly generated string.
    """
    return "".join(random.choice(string.ascii_letters) for _ in range(length))


def random_hex(length: int) -> str:
    """Generates a random hexadecimal string of specified length.
    
    Args:
        length (int): The length of the random hexadecimal string to generate.
    
    Returns:
        str: A randomly generated hexadecimal string.
    """
    return "%030x" % random.randrange(16**length)


def create_users(n: int = 30) -> List[dict]:
    """Creates n random users.

    Generates a list of random users with unique IDs and usernames based on generated names.
    Each user dictionary includes '_id', 'username', 'name', and 'rooms' fields. The 'rooms' field
    is initially an empty list.

    Args:
        n (int, optional): The number of users to create. Defaults to 30.

    Returns:
        List[dict]: A list of dictionaries representing users.
    """
    users = []
    for i in range(n):
        username = generate_name()
        users.append(
            {
                "_id": random_string(16),
                "username": username,
                "name": username.replace("_", " ").title(),
                "rooms": [],
            }
        )
    return users


def create_rooms(users: List[dict], n: int = 30) -> List[dict]:
    """Creates n random rooms.

    Generates a list of random rooms, each containing a random subset of users. 
    The type of room is randomly chosen from public channels ("c"), private channels ("p"), 
    or direct messages ("d"). Each room is assigned a random creator from the users list.

    Args:
        users (List[dict]): A list of dictionaries representing users, where each user 
            contains information such as '_id' and 'rooms'.
        n (int, optional): The number of rooms to create. Defaults to 30.

    Returns:
        List[dict]: A list of dictionaries representing rooms, where each room contains 
            '_id', 'name', 'users', 'type', 'creator', and 'messages' fields.
    """
    rooms = []
    # edge case could be for example an empty room
    for i in range(n):
        users_in_room = random.sample(users, random.randint(1, len(users)))
        room_name = generate_name().title().split("_")[0]
        room = {
            "_id": random_string(16),
            "name": f"Room {room_name}",
            "users": users_in_room,
            "type": random.choice(["c", "p", "d"]),
            "creator": users_in_room[0],
            "messages": [],
        }
        rooms.append(room)
        for user in users_in_room:
            user["rooms"].append(room)

    return rooms


# TODO REFACTOR
def create_message_events_for_room(room: dict) -> List[dict]:
    """Generates message events for a given room.

    Creates message events based on the type of room. For public channels ("c"), 
    it generates 'user joined' events for each user in the room. For private channels ("p"), 
    it generates 'user added' events for each user except the room creator. For direct 
    messages ("d") or live chats ("l"), no events are generated.

    Args:
        room (dict): A dictionary representing the room, containing information such as '_id', 
            'type', 'users', and 'creator'.

    Returns:
        List[dict]: A list of dictionaries representing message events, where each event contains 
            '_id', 'rid', 'type', 'user', and 'msg' fields.
    """
    # maybe shuffle the lists here, but it might be irrelevant
    # before implementing the dates properly
    message_events = []

    if room["type"] == 'c':
        for user in room["users"]:
            message = {
                "_id": random_string(16),
                "rid": room["_id"],
            }
            message["type"] = "uj"
            message["user"] = user
            message_events.append(message)
            room["messages"].append(message)
    elif room['type'] == "p":
        for user in room["users"]:
            if user["_id"] != room["creator"]["_id"]:
                message = {
                    "_id": random_string(16),
                    "rid": room["_id"],
                }
                message["type"] = "au"
                message["user"] = room["creator"]
                message["msg"] = user["username"]
                message_events.append(message)
                room["messages"].append(message)
    elif room['type'] in  ["d", "l"]:
        pass

    return message_events


def create_messages(rooms: List[dict], n: int = 50) -> List[dict]:
    """Generates messages for each room.

    Iterates over the provided list of rooms, generating messages for each room
    based on the number specified. Each message is randomly generated using a Markov chain
    or a default message if Markov chain fails.

    Args:
        rooms (List[dict]): A list of dictionaries representing rooms, where each room
            contains information such as '_id' and 'users'.
        n (int, optional): The number of messages to generate for each room. Defaults to 50.

    Returns:
        List[dict]: A list of dictionaries representing messages, where each message contains
            '_id', 'rid', 'user', and 'msg' fields.
    """
    global GLOB_COUNT
    messages = []

    for room in rooms:
        # TODO r type
        # TODO MOVE LOOP out of funtion and append single elements
        messages += create_message_events_for_room(room)
        # print(messages)
        for i in range(n):
            msg = f"Message {GLOB_COUNT} <3"
            GLOB_COUNT += 1
            # if text_model:
            #     msg = text_model.make_sentence()
            message = {
                "_id": random_string(16),
                "rid": room["_id"],
                "user": random.choice(room["users"]),
                "msg": msg,
            }
            messages.append(message)
            room["messages"].append(message)
    return messages


def write_users_json(users: List[dict], path: pathlib.Path) -> None:
    """Writes user data to a JSON file.

    Writes the user data provided in the form of a list of dictionaries to a JSON file
    located at the specified path. Each user's data is formatted according to the RocketChat
    structure including various fields such as '_id', 'createdAt', 'services', 'emails', etc.

    Args:
        users (List[dict]): A list of dictionaries representing user data.
        path (pathlib.Path): The path where the JSON file will be written.

    Returns:
        None
    """
    out_path = path / "users.json"
    with open(out_path, "w") as f:
        for user in users:
            email = f"{user['username']}@example.com"
            user_data = {
                "_id": user["_id"],
                "createdAt": {
                    "$date": {
                        "$numberLong": DATE_2022,
                    },
                },
                "services": {
                    "password": {
                        "bcrypt": random_string(16),
                        "enroll": {
                            "token": random_string(16),
                            "email": email,
                            "when": {
                                "$date": {
                                    "$numberLong": DATE_2022,
                                },
                            },
                            "reason": "enroll",
                        },
                    },
                    "email2fa": {
                        "enabled": True,
                        "changedAt": {
                            "$date": {
                                "$numberLong": DATE_2022,
                            }
                        },
                    },
                    "email": {
                        "verificationTokens": [
                            {
                                "token": random_string(16),
                                "address": email,
                                "when": {
                                    "$date": {
                                        "$numberLong": DATE_2022,
                                    }
                                },
                            }
                        ]
                    },
                    "resume": {
                        "loginTokens": [
                            {
                                "when": {
                                    "$date": {
                                        "$numberLong": DATE_2022,
                                    }
                                },
                                "hashedToken": random_string(32),
                                "twoFactorAuthorizedHash": random_hex(32),
                                "twoFactorAuthorizedUntil": {
                                    "$date": {
                                        "$numberLong": DATE_2022,
                                    }
                                },
                            }
                        ]
                    },
                },
                "emails": [
                    {
                        "address": email,
                        "verified": bool(random.getrandbits(1)),
                    },
                ],
                "type": "user",
                "status": "online",
                "active": True,
                "_updatedAt": {
                    "$date": {
                        "$numberLong": DATE_2022,
                    }
                },
                "roles": random.sample(ROLES, random.randint(1, len(ROLES))),
                "name": user["name"],
                "lastLogin": {
                    "$date": {
                        "$numberLong": DATE_2022,
                    }
                },
                "statusConnection": "online",
                "username": user["username"],
                "__rooms": user["rooms"],
                "utcOffset": {
                    "$numberInt": "1",
                },
                "settings": {
                    "preferences": {
                        "sidebarViewMode": "medium",
                        "sidebarDisplayAvatar": bool(random.getrandbits(1)),
                    }
                },
            }
            user_data["__rooms"] = [room["_id"] for room in user["rooms"]]
            # TODO convert to mongo format (no comma and new json on every line)
            f.write(json.dumps(user_data))
            f.write("\n")


def write_rooms_json(rooms: List[dict], path: pathlib.Path) -> None:
    """Writes room data to a JSON file.

    Writes the data of provided rooms to a JSON file located at the specified path.
    Each room's data includes fields such as '_id', 't', 'usernames', 'usersCount', etc.
    Additionally, it includes information about the last message in each room.

    Args:
        rooms (List[dict]): A list of dictionaries representing room data.
        path (pathlib.Path): The path where the JSON file will be written.

    Returns:
        None
    """
    out_path = path / "rocketchat_room.json"
    with open(out_path, "w") as f:
        for room in rooms:
            last_message = room["messages"][-1]
            room_data = {
                "_id": room["_id"],
                "t": room["type"],
                "usernames": [user["username"] for user in room["users"]],
                "usersCount": {
                    "$numberInt": len(room["users"]),
                },
                "msgs": {
                    "$numberInt": len(room["messages"]),
                },
                "ts": {
                    "$date": {
                        "$numberLong": DATE_2023,
                    },
                },
                "uids": [user["_id"] for user in room["users"]],
                "default": False,
                "ro": False,
                "sysMes": True,
                "_updatedAt": {
                    "$date": {
                        "$numberLong": DATE_2023,
                    },
                },
                "_USERNAMES": [user["username"] for user in room["users"]],
                "lastMessage": {
                    "_id": last_message["_id"],
                    "rid": room["_id"],
                    "msg": last_message["msg"],
                    "ts": {
                        "$date": {
                            "$numberLong": DATE_2024,
                        },
                    },
                    "u": {
                        "_id": last_message["user"]["_id"],
                        "username": last_message["user"]["username"],
                        "name": last_message["user"]["name"],
                    },
                    "_updatedAt": {
                        "$date": {
                            "$numberLong": DATE_2024,
                        },
                    },
                    "urls": [],
                    "mentions": [],
                    "channels": [],
                    "md": [
                        {
                            "type": "PARAGRAPH",
                            "value": [
                                {"type": "PLAIN_TEXT", "value": last_message["msg"]}
                            ],
                        }
                    ],
                },
                "lm": {
                    "$date": {
                        "$numberLong": DATE_2024,
                    },
                },
            }
            f.write(json.dumps(room_data))
            f.write("\n")


def write_messages_json(messages: List[dict], path: pathlib.Path) -> None:
    """Writes room data to a JSON file.

    Writes the room data provided in the form of a list of dictionaries to a JSON file
    located at the specified path. Each room's data is formatted according to the RocketChat
    structure including various fields such as '_id', 't', 'usernames', 'msgs', etc.

    Args:
        rooms (List[dict]): A list of dictionaries representing room data.
        path (pathlib.Path): The path where the JSON file will be written.

    Returns:
        None
    """
    out_path = path / "rocketchat_message.json"
    with open(out_path, "w") as f:
        for message in messages:
            message_data = {
                "_id": message["_id"],
                "rid": message["rid"],
                "ts": {
                    "$date": {
                        "$numberLong": DATE_2024,
                    },
                },
                "groupable": False,
                "_updatedAt": {
                    "$date": {
                        "$numberLong": DATE_2024,
                    },
                },
            }
            user = {
                "_id": message["user"]["_id"],
                "username": message["user"]["username"],
            }

            # the msg is also used for some types
            if "msg" in message:
                message_data["msg"] = message["msg"]
            if "type" in message:
                message_data["t"] = message["type"]
            else:
                user["name"] = message["user"]["name"]
                message_data["urls"] = []
                message_data["mentions"] = []
                message_data["channels"] = []
                message_data["md"] = [
                    {
                        "type": "PARAGRAPH",
                        "value": [
                            {
                                "type": "PLAIN_TEXT",
                                "value": message["msg"],
                            }
                        ],
                    }
                ]

            message_data["u"] = user

            f.write(json.dumps(message_data))
            f.write("\n")


# main function
@click.command()
@click.option("--users", default=5)
@click.option("--messages", default=50)
@click.option("--rooms", default=10)
@click.option("--output_dir", default=None)
def cli_main(users: int, messages: int, rooms: int, output_dir: str):
    """Generate test data jsons for RocketChat.

    Generates a user.json, rocketchat_rooms.json and rocketchat_messages.json with random data.

    Args:
        users (int): Number of users to create
        messages (int): Number of messages to create
        rooms (int): Number of rooms to create
        output_dir (str): Path to output dir for output files
    """
    main(users, messages, rooms, output_dir)


def main(users, messages, rooms, output_dir=None):
    users = create_users(n=users)
    rooms = create_rooms(users, n=rooms)
    messages = create_messages(rooms, n=messages)

    if output_dir is None:
        path = pathlib.Path.home()
    else:
        path = Path(output_dir)
    print(f"Generate data in {path.resolve()} ...")

    write_users_json(users, path)
    write_rooms_json(rooms, path)
    write_messages_json(messages, path)


if __name__ == "__main__":
    cli_main()
