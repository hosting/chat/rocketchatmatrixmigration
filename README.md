# RocketChat Test Data Generator
This repository contains a Python script generate_data.py that generates test data for RocketChat. The script creates mock users, rooms, and messages in JSON format for testing purposes.

## Installation

```
git clone https://git.fairkom.net/hosting/chat/rocketchatmatrixmigration.git
cd rocketchatmatrixmigration
virtualenv Rocket2Matrix
source ./Rocket2Matrix/bin/env
pip install -e . # Dev
pip install .    # Prod
```

Or with Poetry:

```
pipx install poetry
poetry install
poetry shell
```




## Usage
To generate test data, run the script with the desired parameters. The script accepts the following options:


--users: Number of users to create (default: 5)

--messages: Number of messages per room to create (default: 50)

--rooms: Number of rooms to create (default: 10)

--output_dir: Path to the output directory for generated JSON files (default: current directory)



## Exmaple Usage

```
python rocket_chat_test_data.py --users 10 --messages 100 --rooms 20 --output_dir /path/to/output
```


